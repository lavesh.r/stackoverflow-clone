<?php

namespace App\Providers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use App\Policies\AnswerPolicy;
use App\Policies\QuestionPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        Question::class => QuestionPolicy::class,
        Answer::class => AnswerPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */

     /*
        In my words
     Gates are nothing but a security checkpoint where it checks whethever the user is permitted to perform the following task
     it takes 2 arguments User and Model
     User: it is the user which is going to access method of the model.

     At the time of defination of Gates we need to return boolean value which decides whether the user is permitted or not.
     */

    public function boot()
    {
        $this->registerPolicies();

        //User Defined Gates

        // Gate::define('update-question',function(User $user,Question $question)
        // {
        //     return $user->id === $question->id;
        // });

        // Gate::define('delete-question',function(User $user,Question $question)
        // {
        //     return $user->id === $question->id;
        // });

    }
}
