<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;
use App\Helpers\Votable;

class Question extends Model
{
    use HasFactory;
    use Votable;

    protected $guarded = ['id'];

    public function setTitleAttribute(String $title)
    {
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }

    public function markBestAnswer(Answer $answer)
    {
        $this->best_answere_id == $answer->id;
        $this->save();
    }
    public function owner()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
    public function favorites()
    {
        return $this->BelongsToMany(User::class)->withTimestamps();
    }
    //Accessors
    public function getUrlAttribute()
    {
        return "questions/{$this->slug}";
    }
    public function getCreatedDateAttribute()
    {
        return $this->created_at->diffForHumans();
    }
    public function getAnswerStyleAttribute()
    {
        if($this->answers_count>0)
        {
            if($this->best_answer_id > 0 )
            {
                return "has-best-answer";
            }
            return "answered";
        }
        return "unanswered";
    }


    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }
    public function getIsFavoriteAttribute()
    {
        return $this->favorites()->where('user_id',auth()->id())->count() > 0;
    }
    public function getFavoriteStyleAttribute()
    {
        if($this->getIsFavoriteAttribute())
        {
            return "text-success";
        }
        return "text-black-50";
    }

    //votes

    public function votes()
    {
        return $this->morphToMany(User::class,'vote')->withTimestamps();
    }

}
