<?php

namespace Database\Seeders;

use App\Models\Answer;
use App\Models\Question;
use App\Models\User;
use Database\Factories\QuestionFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(5)->create()->each(function(User $user){
        //     // for($i = 1;$i<=rand(5,10);$i++ )
        //     // {
        //     //     $user->questions()->create(Question::factory()->make()->toArray());
        //     // }
        //     //The Above Query was running Query 5 to 10 rand times
        //     // we can make it more efficient
        //     $user->questions()->saveMany(
        //         Question::factory(rand(2,5))->make())
        //     ->each(function(Question $question)
        //     {
        //         $question->answers()->saveMany(Answer::factory(rand(2,7))->make());
        //     });
        // });

        User::factory(5)->create()->each(function($user) {
            $user->questions()
                ->saveMany(
                    Question::factory(rand(2, 5))->make()
                )
                ->each(function(Question $question) {
                    $question->answers()
                    ->saveMany(Answer::factory(rand(2, 6))->make());
                });
        });
    }


}
