<div class="row mt-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="">{{Illuminate\Support\Str::plural('Answer', $question->answers_count)}}</h3>
            </div>
            <div class="card-body">
                @foreach ($question->answers as $answer )
                    {!! $answer->body !!}
                    <div class="d-flex justify-content-between mr-3 mt-2">
                        <div class="d-flex">
                            <div class="ml-2">
                                <a href="" title="Vote-up" class="vote-up d-block text-center text-black-50">
                                    <i class="fa fa-caret-up fa-3x"></i>
                                </a>
                                <h4 class="vote-count text-muted text-center m-0">45</h4>
                                <a href="" title="Vote-down" class="vote-down d-block text-center text-black-50">
                                    <i class="fa fa-caret-down fa-3x"></i>
                                </a>
                            </div>
                            <div class="ml-5 mt-3">
                                @can('markAsBest',$answer)
                                    <form action="{{route('answers.bestAnswer',$answer->id)}}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <button type="submit" class="btn {{$answer->best_answer_style}}" title="Mark as Best Answer">
                                            <i class="fa fa-check fa-2x"></i>
                                        </button>
                                    </form>
                                    @else
                                        @if ($answer->is_best_answer)
                                            <i class="fa fa-check fa-2x text-success d-block mb-2"></i>
                                        @endif
                                @endcan
                                <h4 class="views-count text-muted text-center m-0">45</h4>
                            </div>
                        </div>
                        <div>

                            @can('update',$answer)
                                <a href="{{route('questions.answers.edit',[$question->id,$answer->id])}}" class="btn btn-outline-info">Edit</a>
                            @endcan
                            @can('delete',$answer)
                                <form action="{{route('questions.answers.destroy',[$question->id,$answer->id])}}" method="POST" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-danger" >Delete</button>
                                </form>
                            @endcan
                        </div>
                        <div class="d-flex flex-column">
                            <div class="text-muted mb-2 text-right">
                                <p>Answered {{$answer->created_date}}</p>
                            </div>
                            <div class="mb-2 d-flex">
                                <div><img src="{{$answer->author->avatar}}" alt=""></div>
                                <div class="mt-2 ml-1">
                                    {{$answer->author->name}}
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
</div>
